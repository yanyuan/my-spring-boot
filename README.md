# my-spring-boot

#### 介绍
- Spring Boot 学习参考，本文计划根据作者近几年的工作、学习经验，来分析和制定一个学习使用 Spring Boot技术的步骤。
- SpringBoot是伴随着Spring4.0诞生的； SpringBoot的目标是简化Spring的开发过程、让开发者快速搭建框架和web容器。并为微服务提供更好的支持，提供服务监控能力。Spring为开发者带来了简单和能力：



#### 分支说明

-  **5-jpa** ：[springboot jpa使用](https://blog.csdn.net/yanyuan_1118/article/details/108753605) 
-  **8-mongo** : [Spring Boot数据访问：使用MongoDB](https://blog.csdn.net/yanyuan_1118/article/details/108879596)
-  **data-redis** : [Spring Boot数据访问：Redis应用](https://blog.csdn.net/yanyuan_1118/article/details/109046462)
- **data-mybaits**: [Spring Boot数据访问：整合Mybatis](https://blog.csdn.net/yanyuan_1118/article/details/109049425)
-  **data-transaction** : [Spring Boot数据访问：事务处理](https://blog.csdn.net/yanyuan_1118/article/details/109195741)
-  **data-queue** : [Spring Boot使用消息队列：集成RabbitMQ](https://blog.csdn.net/yanyuan_1118/article/details/109315768)
-  **monogo-transaction** : [Springboot monogoDB事务处理](https://blog.csdn.net/yanyuan_1118/article/details/109624106)
-  **async-task** : [Spring Boot异步任务：@Async](https://blog.csdn.net/yanyuan_1118/article/details/109200434)

#### 其它
- [Spring Boot快速入门：SpringBoot是什么？](https://blog.csdn.net/yanyuan_1118/article/details/108486733)
- [Spring Boot快速入门：配置文件](https://blog.csdn.net/yanyuan_1118/article/details/108713841)
- [Spring Boot配置文件优先级别](https://blog.csdn.net/yanyuan_1118/article/details/108129101)
- [Spring Boot单元测试：GroboUtils多线程测试](https://blog.csdn.net/yanyuan_1118/article/details/107757670)